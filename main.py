from fastapi import FastAPI

from models import Informations, Solution

from solution import solve    

description ="""
Nonograms resolution

"""
tags_metadata = [
    {
        "name": "Info",
        "description": "Post your informations here",
    }
]

app = FastAPI(
    openapi_tags=tags_metadata,
    title="Nonograms solver",
    description=description
)




@app.post("/solve/", response_model=Solution, tags=['Info'] )
async def solve_nono(informations: Informations):
    print('salut')
    return solve(informations)