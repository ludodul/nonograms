from pydantic import BaseModel
from typing import Optional, List

class Informations(BaseModel):
    info_lignes: list
    info_cols: list
    rounds: Optional[int] = 50

    class Config:
        schema_extra = {
            "example": {
                "info_lignes":  [
                    [4],
                    [4],
                    [2,1],
                    [5],
                    [4]
                ],
                "info_cols": [
                    [1,3],
                    [5],
                    [2,2],
                    [5],
                    [1,1]
                ],
                "rounds": 50,
            }
        }

class Solution(BaseModel):
    solution: List[list]
    
    