import numpy as np
from models import Informations, Solution

def list_piece(size, info):
    res = []
    for i in range(size-info+1):
        res.append([-1]*i + [1]*info + [-1]*(size-i-info))
    return res

def poss(size, infos, pref=[]):
    
    liste=[]
    size_min = sum(infos) + len(infos) - 1

    if len(infos) == 1:
        return [pref + l for l in list_piece(size, infos[0])]

    else:
        for i in range(size - size_min + 1):
            liste += poss(size - infos[0] - 1 - i,
                          infos[1:],
                          pref=pref + [-1]*i + infos[0]*[1] + [-1])
        return liste

class Arr:
    def __init__(self, size: int, infos: list):
        self.size = size
        self.possibilites = poss(size, infos)
        self.status = False
    
    def check_status(self):
        if len(self.possibilites) == 1:
            self.status = True

    def reduce_poss(self, ligne: list):
        res = []
        if all(v == 0 for v in ligne):
            return self.possibilites
        s = sum((v!=0 for v in ligne))
        for li in self.possibilites:
            H = li*np.array(ligne)
            if sum(H) == s:
                res.append(li)
        self.possibilites =  np.array(res)
        self.check_status()
    
    def intersection(self):
        return [ int(a/abs(a)*(abs(a)//len(self.possibilites))) if a!=0 else 0 for a in sum(np.array(self.possibilites)) ]

class Nono:
    def __init__(self, infos_lignes, infos_cols):
        #self.size = size
        self.nbr_l = len(infos_lignes)
        self.nbr_c = len(infos_cols)
        self.infos_lignes = infos_lignes
        self.infos_cols = infos_cols
        self.grid = np.array([np.zeros(self.nbr_c, dtype=int) for _ in range(self.nbr_l)])
        self.lignes = [Arr(self.nbr_c, self.infos_lignes[i]) for i in range(self.nbr_l)]
        self.cols = [Arr(self.nbr_l, self.infos_cols[i]) for i in range(self.nbr_c)]
        self.status_lignes = [ligne.status for ligne in self.lignes]
        self.status_cols = [col.status for col in self.cols]
        self.rounds = 0

    def display(self):
        for l in self.grid:
            d = ['\u25a0' if a==1 else '?' if a==0 else 'X' for a in l ]
            print(d)

    def check_end(self):
        self.status_lignes = [ligne.status for ligne in self.lignes]
        self.status_cols = [col.status for col in self.cols]
        return all(self.status_lignes) and all(self.status_cols)

    def apply_lignes(self):
        if not all(self.status_lignes):
            for ind, ligne in enumerate(self.lignes):
                ligne.reduce_poss(self.grid[ind])
                self.grid[ind] = ligne.intersection()

    def apply_cols(self):
        if not all(self.status_cols):
            for ind, col in enumerate(self.cols):
                col.reduce_poss(self.grid[:,ind])
                self.grid[:,ind] = col.intersection()

    def solve(self, rounds=50):
        i = 0
        while i<rounds and not self.check_end():
            self.apply_cols()
            self.apply_lignes()
            i+=1
            self.rounds +=1
        if self.check_end():
            print(f'résolu en {self.rounds} rounds...')
        else:
            print('non résolu, rajoutez quelques rounds...')
        self.display()
        return list(self.grid)
        

    
def solve(informations: Informations)->Solution:
    nono = Nono(
        informations.info_lignes,
        informations.info_cols
        )
    grid = nono.solve()
    h = []
    for l in grid:
        d = ['\u25a0' if a==1 else '?' if a==0 else 'X' for a in l ]
        h.append(d)
    s = Solution(solution=h)
    return s


    


    
        